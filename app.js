const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const routes = require('./routes/index');
const mysql = require('mysql');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(flash());

app.use('/', routes);


//TODOLIST
//Add to todolist  C
app.get('/todolistadd',(req,res)=>{
    let task = req.query.task_todos;
    let sql = `INSERT INTO todolist (id,task) VALUES (NULL,'${task}')`;
    
    let query = conn.query(sql,(err,results)=>{
        res.redirect('/');
    });
});

//Update task  U

//Delete task  D

//Notes
//Add to notes C
app.get('/notesadd',(req,res)=>{
    let note = req.query.note;
    let sql = `INSERT INTO notes (id_note,note) VALUES (NULL,'${note}')`;

    let query = conn.query(sql,(err,results)=>{
        res.redirect('/');
    });
});
//Update notes  U

//Delete notes  D

module.exports = app;