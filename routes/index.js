const express = require('express');
const router = express.Router();
const mysql = require('mysql');
//connect database
const config = {
   host     : 'localhost',
   user     : 'root',
   password : '',
   database : 'przybornik',
}
const conn = mysql.createConnection(config);
//TODOLIST View
router.get('/',(req,res)=>{
   let sql = `SELECT * FROM todolist`;
   
   let query = conn.query(sql,(err,results)=>{
      console.log(results);
       res.render('index', {
           results: results
       });
   });
});

module.exports = router;